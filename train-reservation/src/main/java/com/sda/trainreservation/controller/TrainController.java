package com.sda.trainreservation.controller;
import com.sda.trainreservation.datasource.Train;
import com.sda.trainreservation.repository.StationRepository;
import com.sda.trainreservation.service.TrainService;
import com.sda.trainreservation.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.annotation.security.RolesAllowed;
import java.util.ArrayList;

@Controller
@RequestMapping("/trains")
public class TrainController {
    @Autowired
    private TrainService trainService;

    @Autowired
    private StationRepository stationRepository;

    @GetMapping("/findAll")
    @RolesAllowed({"ROLE_USER","ROLE_ADMIN"})
    public String findAll(Model model) {
        ArrayList<Train> trains = (ArrayList<Train>) trainService.findAll();
        for (Train train : trains) {
            train.setTravelTime(Utils.millisToString(train.getTravelTimeMillis()));
            train.setDepartureTime(Utils.millisToString(train.getDepartureDate()));
        }
        model.addAttribute("allTrains", trains);
        System.out.println(trainService.findAll());
        return "trains";
    }

    @GetMapping("/addNewTrain")
    @RolesAllowed({"ROLE_ADMIN"})
    public String addNewTrainForm(Model model) {
        Train train = new Train();
        model.addAttribute("allStations", stationRepository.findAll());
        model.addAttribute("train", train);
        return "new_train";
    }

    @PostMapping("/addTrain")
    @RolesAllowed({"ROLE_ADMIN"})
    public String addTrain(@ModelAttribute("train") Train train, @RequestParam("departureTime") String departureTime,@RequestParam("travelTime") String travelTime) {
        // Train train= (Train) model.getAttribute("train");
        train.setDepartureDate(Utils.stringToMillis(departureTime));
        train.setTravelTimeMillis(Utils.stringToMillis(travelTime));
        trainService.addTrain(train);
        return "redirect:/trains/findAll";
    }

    @PutMapping("/updateTrain/{id}")
    @RolesAllowed({"ROLE_ADMIN"})
    public String updateTrain(@ModelAttribute("train") Train train, @RequestParam("departureTime") String departureTime,@RequestParam("travelTime") String travelTime) {
        // Train train= (Train) model.getAttribute("train");
        train.setDepartureDate(Utils.stringToMillis(departureTime));
        train.setTravelTimeMillis(Utils.stringToMillis(travelTime));
        trainService.saveTrain(train);
        return "redirect:/trains/findAll";
    }

    @PostMapping("/delete/{id}")
    @RolesAllowed({"ROLE_ADMIN"})
    public String delete(@PathVariable(name = "id") Integer id) {
        trainService.delete(id);
        return "redirect:/trains/findAll";
    }
}
