package com.sda.trainreservation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RoleController {
    @GetMapping("/")
    public String home() {
        return "home";
    }

    @GetMapping("/user")
    public String user() {
        return ("<h1>Welcome User!</h1>");
    }

    @GetMapping("/admin")
    public String admin() {
        return ("<h1>Welcome admin!</h1>");
    }

}
