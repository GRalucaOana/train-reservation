package com.sda.trainreservation.controller;

import com.sda.trainreservation.datasource.User;
import com.sda.trainreservation.repository.PrivilegeRepository;
import com.sda.trainreservation.repository.RoleRepository;
import com.sda.trainreservation.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.Collections;

@Controller
//@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    //create read update delete

    @GetMapping(value = "/addRegisterUser")
    @RolesAllowed({"ROLE_USER"})
    public String showForm(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "userRegistration";
    }

    @PostMapping("/submitRegister")
    @RolesAllowed({"ROLE_USER"})
    public String submitForm(@ModelAttribute("user") User user) {
        System.out.println(user);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        com.sda.trainreservation.datasource.Role userRole = roleRepository.findByName("ROLE_USER");
        user.setRoles(Collections.singletonList(userRole));
        userService.addUser(user);

        return "register_success";
    }


}


