package com.sda.trainreservation.controller;
import com.sda.trainreservation.datasource.Reservation;
import com.sda.trainreservation.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

@Controller
//@RequestMapping("/reservations")
public class ReservationController {
    @Autowired
    private ReservationService reservationService;

    @GetMapping("/findAllReservations")
    public String findAll(Model model) {
        model.addAttribute("allReservations", reservationService.findAll());
        System.out.println(reservationService.findAll());
        // return reservationService.findAll();
        return "reservation";
    }

//    @PostMapping("/addReservation")
//    public Reservation add(@RequestBody ReservationRequest reservation) {
//        return reservationService.add(reservation);
//    }
//    @GetMapping("/showReservation")
//    public Reservation showAddReservation(Model model){
//        model.addAttribute("newReservation", new Reservation());
//        System.out.println();
//    }

    @GetMapping(value = "/showAddReservation")
    public ModelAndView showForm() {
        return new ModelAndView("add_reservation","reservation", new Reservation());
    }

    @PostMapping(value = "/addReservation")
    public String submit(@Valid @ModelAttribute("reservation")Reservation reservation,
                         BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }
        model.addAttribute("train", reservation.getTrain());
        model.addAttribute("seat", reservation.getSeat());
        model.addAttribute("ticket_price", reservation.getTicketPrice());
//        model.addAttribute("user", reservation.getUser());
        return "add_reservation";
    }

    @PutMapping("/updateReservation/{id}")
    @RolesAllowed({"ROLE_USER"})
    public String updateReservation(@ModelAttribute("reservation") Reservation reservation) {
        // Train train= (Train) model.getAttribute("train");
        reservationService.saveReservation(reservation);
        return "redirect:/showAddReservation";
    }

//    @DeleteMapping("/{id}")
//    public void delete(@PathVariable(name = "id") Integer id) {
//        reservationService.delete(id);
//    }

//    @PutMapping("/addTicket")
//    public Reservation addTicket(@RequestBody Integer ticketPrice){
//        return reservationService.addTicket(ticketPrice);
//}

    @PostMapping("/delete/{id}")
    @RolesAllowed({"ROLE_USER"})
    public String delete(@PathVariable(name = "id") Integer id) {
        reservationService.delete(id);
        return "redirect://trains/findAll";
    }
}
