package com.sda.trainreservation.controller;

import com.sda.trainreservation.datasource.Station;
import com.sda.trainreservation.service.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/stations")
public class StationController {
    @Autowired
    private StationService stationService;

    @GetMapping("/findAll")
    public List<Station>findAll(@RequestParam(name="name", required = false) String name){
        return stationService.findAll(name);
    }

//    @PostMapping
//    public Station create(@RequestBody Station station){
//        return stationService.create(station);
//    }

    @PutMapping("/addStation")
    public Station add(@RequestBody Station station){
        return stationService.add(station);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable(name = "id") Integer id){
       stationService.delete(id);
    }


}
