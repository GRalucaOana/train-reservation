package com.sda.trainreservation.util;

import java.util.concurrent.TimeUnit;

public class Utils {
    public static String millisToString(long millis) {
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        long minsToSubstract = TimeUnit.HOURS.toMinutes(hours);
        long mins = TimeUnit.MILLISECONDS.toMinutes(millis) - minsToSubstract;
        return "" + hours + ":" + (mins < 10 ? "0" : "") + mins;
    }

    public static long stringToMillis(String millis) {
        //18:45
        String[] textparts = millis.split(":");
        int hours =Integer.parseInt(textparts[0]);
        int minutes= Integer.parseInt(textparts[1]);
        long ms= TimeUnit.HOURS.toMillis(hours)+TimeUnit.MINUTES.toMillis(minutes);
        return ms;
    }
}
