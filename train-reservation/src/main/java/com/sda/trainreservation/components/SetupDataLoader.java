//package com.sda.trainreservation.components;
//
//import com.sda.trainreservation.datasource.*;
//import com.sda.trainreservation.repository.*;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationListener;
//import org.springframework.context.event.ContextRefreshedEvent;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.stereotype.Component;
//
//
//import javax.transaction.Transactional;
//import java.util.Arrays;
//import java.util.Collection;
//import java.util.Collections;
//import java.util.List;
//
//@Component
//public class SetupDataLoader implements
//        ApplicationListener<ContextRefreshedEvent> {
//
//    boolean alreadySetup = false;
//
//    @Autowired
//    private UserRepository userRepository;
//
//    @Autowired
//    private RoleRepository roleRepository;
//
//    @Autowired
//    private PrivilegeRepository privilegeRepository;
//
//    @Autowired
//    private PasswordEncoder passwordEncoder;
//
//    @Autowired
//    private StationRepository stationRepository;
//
//    @Autowired
//    private TrainRepository trainRepository;
//
//    @Override
//    @Transactional
//    public void onApplicationEvent(ContextRefreshedEvent event) {
//
//        if (alreadySetup)
//            return;
//        Privilege readPrivilege
//                = createPrivilegeIfNotFound("READ_PRIVILEGE");
//        Privilege writePrivilege
//                = createPrivilegeIfNotFound("WRITE_PRIVILEGE");
//
//        List<Privilege> adminPrivileges = Arrays.asList(
//                readPrivilege, writePrivilege);
//        createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
//        createRoleIfNotFound("ROLE_USER", Collections.singletonList(readPrivilege));
//
//        com.sda.trainreservation.datasource.Role adminRole = roleRepository.findByName("ROLE_ADMIN");
//        User user = new User();
//        user.setUsername("Administrator");
//        user.setPassword(passwordEncoder.encode("test"));
//        user.setRoles(Collections.singletonList(adminRole));
//        userRepository.save(user);
//
////        alreadySetup = true;
//
//
//        createStationIfNotFound("Cluj-Napoca");
//        createStationIfNotFound("Timisoara");
//        createStationIfNotFound("Oradea");
//        createStationIfNotFound("Baia Mare");
//        createStationIfNotFound("Bucuresti");
//        createStationIfNotFound("Mangalia");
//
//        createTrainIfNotFound("IR5368");
//
//       // train.setAllSeats();
//
//    }
//
//    @Transactional
//    Privilege createPrivilegeIfNotFound(String name) {
//
//        Privilege privilege = privilegeRepository.findByName(name);
//        if (privilege == null) {
//            privilege = new Privilege();
//            privilege.setName(name);
//            privilegeRepository.save(privilege);
//        }
//        return privilege;
//    }
//
//    @Transactional
//    Train createTrainIfNotFound(String name) {
//
//        Train existingtrain = trainRepository.findByName(name);
//        if (existingtrain == null) {
//            Train train =new Train();
//            train.setName(name);
//            train.setDepartureStation(stationRepository.findAll().get(0));
//            train.setDepartureDate((long) 32400000);
//            train.setArrivalStation(stationRepository.findAll().get(1));
//            train.setTravelTimeMillis((long) 7200000);
//            train.setNrFreeSeats(10);
//            train.setAllSeats(75);
//            train.setDistance(200);
//            trainRepository.save(train);
//            return train;
//        }
//        return existingtrain;
//    }
//
//    @Transactional
//   Station createStationIfNotFound(String name) {
//
//        Station existingstation = stationRepository.findByName(name);
//        if (existingstation == null) {
//            Station station=new Station();
//            station.setName(name);
//            stationRepository.save(station);
//            return station;
//        }
//        return existingstation;
//    }
//
//
//    @Transactional
//    Role createRoleIfNotFound(
//            String name, Collection<Privilege> privileges) {
//
//        Role role = roleRepository.findByName(name);
//        if (role == null) {
//            role = new Role();
//            role.setName(name);
//            role.setPrivileges(privileges);
//            roleRepository.save(role);
//        }
//        return role;
//    }
//}
