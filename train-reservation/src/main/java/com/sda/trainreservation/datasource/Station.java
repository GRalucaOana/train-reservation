package com.sda.trainreservation.datasource;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "stations")
public class Station {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @OneToMany(mappedBy = "arrivalStation")
    private List<Train> arrivingTrains;

    @OneToMany(mappedBy = "departureStation")
    private List<Train> departingTrains;

    @Column(name = "name")
    private String name;

    public List<Train> getArrivingTrains() {
        return arrivingTrains;
    }

    public void setArrivingTrains(List<Train> arrivingTrains) {
        this.arrivingTrains = arrivingTrains;
    }

    public List<Train> getDepartingTrains() {
        return departingTrains;
    }

    public void setDepartingTrains(List<Train> departingTrains) {
        this.departingTrains = departingTrains;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
