package com.sda.trainreservation.datasource;
import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "trains")
public class Train {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    private Station departureStation;


    @Column(name = "departure_hour")
//   @Temporal(TemporalType.TIME)
//    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private Long departureDate;

    @Transient
    private String departureTime;

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    private Station arrivalStation;

    @Column
    private Long travelTimeMillis;

    public String getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(String travelTime) {
        this.travelTime = travelTime;
    }

    @Transient
    private String travelTime = "";

    @Column
    private Integer distance;

    @Column(name = "all_seats")
    private Integer allSeats;

    @Column(name = "nr_free_seats")
    private Integer nrFreeSeats;

    @OneToMany(mappedBy = "train")
    private Set<Reservation> reservations;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Station getDepartureStation() {
        return departureStation;
    }

    public void setDepartureStation(Station departureStation) {
        this.departureStation = departureStation;
    }

    public Long getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Long departureDate) {
        this.departureDate = departureDate;
    }

    public Station getArrivalStation() {
        return arrivalStation;
    }

    public void setArrivalStation(Station arrivalStation) {
        this.arrivalStation = arrivalStation;
    }

    public Long getTravelTimeMillis() {
        return travelTimeMillis;
    }

    public void setTravelTimeMillis(Long travelTimeMillis) {
        this.travelTimeMillis = travelTimeMillis;
    }


    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Integer getDistance() {
        return distance;
    }

    public Integer getAllSeats() {
        return allSeats;
    }

    public void setAllSeats(Integer allSeats) {
        this.allSeats = allSeats;
    }

    public Integer getNrFreeSeats() {
        return nrFreeSeats;
    }

    public void setNrFreeSeats(Integer nrFreeSeats) {
        this.nrFreeSeats = nrFreeSeats;
    }

    public Set<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(Set<Reservation> reservations) {
        this.reservations = reservations;
    }
}
