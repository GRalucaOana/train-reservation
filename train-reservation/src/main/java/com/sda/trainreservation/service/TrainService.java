package com.sda.trainreservation.service;
import com.sda.trainreservation.datasource.Train;
import com.sda.trainreservation.repository.TrainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrainService{
    @Autowired
    private TrainRepository trainRepository;

    public List<Train> findAll() {
        return trainRepository.findAll();
    }

    public void addTrain(Train train) {
        this.trainRepository.save(train);
    }

    public void saveTrain(Train train) {
        this.trainRepository.save(train);
    }

    public void delete(Integer id){
        trainRepository.deleteById(id);
    }

}




//    public Train create(Train train){
//      return trainRepository.save(train);
//    }