package com.sda.trainreservation.service;

import com.sda.trainreservation.datasource.Station;
import com.sda.trainreservation.exception.FiledIsMandatoryException;
import com.sda.trainreservation.repository.StationRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StationService {
    @Autowired
    private StationRepository stationRepository;
    public List<Station> findAll(String name) {
        return stationRepository.findAll();
    }

    public Station add(Station station) {
        validate(station);
        return stationRepository.save(station);
    }

    private void validate(Station station) {
        if (Strings.isEmpty(station.getName())) {
            throw new FiledIsMandatoryException("City is mandatory!");
        }
    }

//    public Station create(Station station) {
//
//    }

    public void delete(Integer id) {
        stationRepository.deleteById(id);
    }
}
