package com.sda.trainreservation.service;

import com.sda.trainreservation.datasource.Reservation;
import com.sda.trainreservation.datasource.Train;
import com.sda.trainreservation.datasource.User;
import com.sda.trainreservation.exception.FiledIsMandatoryException;
import com.sda.trainreservation.repository.ReservationRepository;
import com.sda.trainreservation.repository.TrainRepository;
import com.sda.trainreservation.repository.UserRepository;
import com.sda.trainreservation.type.ReservationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReservationService {
    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TrainRepository trainRepository;

    public List<Reservation> findAll() {
        return reservationRepository.findAll();
    }

    public Reservation add(ReservationRequest reservationRequest) {
        Reservation reservation = new Reservation();
        reservation.setSeat(reservationRequest.getSeat());

        Optional<User> user = userRepository.findById(reservationRequest.getUserId());
        if (user.isPresent()) {
            reservation.setUser(user.get());
        } else {
             throw new FiledIsMandatoryException("user not found!"); //Todo: throw new exception => user not found
        }

        Optional<Train> train = trainRepository.findById(reservationRequest.getTrainId());
        if (train.isPresent()) {
            Train trainObject = train.get();
            trainObject.setNrFreeSeats(trainObject.getNrFreeSeats() - reservationRequest.getSeat());
            reservation.setTrain(trainObject);
        } else {
            // Todo: throw new exception => train not found
        }

        reservation.setSeat(reservationRequest.getSeat());
        reservation.setTicketPrice(0);//Todo: add a method to compute ticket price

        reservationRepository.save(reservation);
        return reservation;
    }

    public void delete(Integer id) {
        reservationRepository.deleteById(id);
    }

    public void saveReservation(Reservation reservation) {
        this.reservationRepository.save(reservation);
    }


}
