package com.sda.trainreservation.service;
import com.sda.trainreservation.datasource.Train;
import com.sda.trainreservation.datasource.User;
import com.sda.trainreservation.exception.FiledIsMandatoryException;
import com.sda.trainreservation.repository.UserRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    //private BCryptPasswordEncoder bCryptPasswordEncoder;

    public void addUser(User user) {
        this.userRepository.save(user);

    }
//    public User addUser(User user) {
//        validate(user);
//    return userRepository.save(user);
//    }

    private void validate(User user) {
        if (Strings.isEmpty(user.getUsername())) {
            throw new FiledIsMandatoryException("Username is mandatory!");
        }
        if (Strings.isEmpty(user.getPassword())) {
            throw new FiledIsMandatoryException("Password is mandatory!");
        }
    }
    public List<User> findAll() {
        return userRepository.findAll();
    }

    public void delete(Integer id){
        userRepository.deleteById(id);
    }
}
