package com.sda.trainreservation.repository;

import com.sda.trainreservation.datasource.Train;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainRepository extends JpaRepository<Train,Integer> {
    List<Train>findAll();
    Train findByName(String name);

}










//    String addTrain();
//Train findByDepartureStation(String departureStation);
//Train findByName(String name);

