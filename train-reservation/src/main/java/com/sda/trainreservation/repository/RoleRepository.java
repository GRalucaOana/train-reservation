package com.sda.trainreservation.repository;

import com.sda.trainreservation.datasource.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role,Integer> {
   Role findByName(String role_admin);
}
