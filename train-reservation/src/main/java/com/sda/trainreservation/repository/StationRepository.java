package com.sda.trainreservation.repository;

import com.sda.trainreservation.datasource.Station;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StationRepository extends JpaRepository<Station,Integer> {

    Station findByName(String name);
}
