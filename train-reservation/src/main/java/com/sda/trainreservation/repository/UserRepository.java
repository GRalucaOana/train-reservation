package com.sda.trainreservation.repository;

import com.sda.trainreservation.datasource.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
//void addUser();
   User findByUsername(String username);
//Optional<User> findById(Integer id);
}
