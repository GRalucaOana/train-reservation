package com.sda.trainreservation.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private CustomDetailService customUserDetailService;

//        @Override
//        protected void configure(HttpSecurity http) throws Exception {
//            http
//                   .authorizeRequests()
//                    .antMatchers(HttpMethod.POST, "/**").permitAll()
//                    .antMatchers("/static/**").permitAll() //make css file public

//                    .antMatchers(HttpMethod.GET,
//                            "/registration",
//                            "/login",
//                            "/forgetPassword",
//                            "/resetPassword").permitAll()
//                    .anyRequest().authenticated()
//                    .and()
//
//                    .formLogin()
//                   .loginPage("/login")
//                   .usernameParameter("username")
//                   .permitAll()
//                .and()
//
//                .logout()
//                .and()
//                .formLogin()
//                .loginPage("/login");
//                .loginProcessingUrl("/perform_login")
//                .defaultSuccessUrl("/home.html", true)
//                .failureUrl("/login.html?error=true")
//                .failureHandler(authenticationFailureHandler())
//                .and()
//                .logout()
//                .logoutUrl("/perform_logout")
//                .deleteCookies("JSESSIONID");
//                .logoutSuccessHandler(logoutSuccessHandler());
    //       }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/admin").hasRole("ADMIN")
                .antMatchers("/user").hasAnyRole("ADMIN", "USER")
                .antMatchers("/", "/login").permitAll()
                .antMatchers(HttpMethod.GET,
                        "/",
                        "/register",
                        "/users/register",
                        "/addRegisterUser",
                        "/login",
                        "/trains",
                        "/trains/**").permitAll()
                .antMatchers(HttpMethod.POST,
                        "/submitRegister"
                        ).permitAll()
                .antMatchers( "/css/**", "/webjars/**").permitAll() //make css file public
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .usernameParameter("username")
                .permitAll()
                .and()
                .csrf().disable();


//                .loginProcessingUrl("/perform_login")
//                .successForwardUrl("/trains/findAll")
//                .defaultSuccessUrl("/trains/findAll", true)
////                .failureUrl("/login.html?error=true")
////                .failureHandler(authenticationFailureHandler())
//                .and()
//                .logout()
////                .logoutUrl("/perform_logout")
//                .deleteCookies("JSESSIONID")
//               // .logoutSuccessHandler(logoutSuccessHandler());
//                .permitAll();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserDetailService)
                .passwordEncoder(bCryptPasswordEncoder());
        super.configure(auth);
    }
}

